5.times do
  post = Post.create(title: Faker::Job.title, content: Faker::Lorem.paragraph)
  3.times do
    post.comments.build(author: Faker::Name.name, content: Faker::Lorem.paragraph)
  end
post.save!
end