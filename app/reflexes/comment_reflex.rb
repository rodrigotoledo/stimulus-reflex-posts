# frozen_string_literal: true

class CommentReflex < ApplicationReflex
  include CableReady::Broadcaster
  before_reflex do
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(comment_params)
  end

  def submit
    morph :false
    @comment.save

    partial_html = CommentsController.render(partial: 'table_row', locals: { comment: @comment })
    cable_ready["comment"].insert_adjacent_html(
      position: "afterbegin",
      selector: "#comments-for-#{@post.id}",
      html: partial_html
    )

    cable_ready.broadcast

    partial_html = CommentsController.render(partial: 'form', locals: { comment: @post.comments.build })
    cable_ready["comment"].inner_html(
      selector: "#comment-form-#{@comment.post_id}",
      html: partial_html
    )

    cable_ready.broadcast
  end

  protected

    def comment_params
      params.require(:comment).permit(:author, :content)
    end
end
