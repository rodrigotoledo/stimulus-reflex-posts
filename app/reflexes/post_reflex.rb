# frozen_string_literal: true
class PostReflex < ApplicationReflex
  include CableReady::Broadcaster
  before_reflex do
    @post = Post.new(post_params)
  end

  def submit
    morph :false
    @post.save

    partial_html = PostsController.render(partial: 'post', locals: { post: @post })
    cable_ready["post"].insert_adjacent_html(
      position: "afterbegin",
      selector: "#posts",
      html: partial_html
    )

    cable_ready.broadcast

    cable_ready["post"].inner_html(
      selector: "#posts-count",
      html: "Total of posts #{Post.count}"
    )

    cable_ready.broadcast

    partial_html = PostsController.render(partial: 'form', locals: { post: Post.new })
    cable_ready["post"].inner_html(
      selector: "#post-form",
      html: partial_html
    )

    cable_ready.broadcast
  end

  protected

    def post_params
      params.require(:post).permit(:title, :content)
    end

end
