class Post < ApplicationRecord
  has_many :comments, autosave: true, dependent: :destroy

  scope :with_cache, -> { select(:id, :title, :content).order(created_at: :desc) }
end
