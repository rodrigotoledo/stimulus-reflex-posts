class Comment < ApplicationRecord
  belongs_to :post
  scope :with_cache, -> { select(:id, :author, :content, :post_id).order(created_at: :desc) }
end
