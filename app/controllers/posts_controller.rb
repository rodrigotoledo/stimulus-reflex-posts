class PostsController < ApplicationController
  include PostsConcern

  before_action :set_posts, only: :index
  
  def index
  end

  def new
    @post = Post.new
  end
end
