module CommentsConcern
  extend ActiveSupport::Concern

  protected

    def current_post
      @current_post ||= Post.with_cache.find(params[:post_id])
    end

    def current_comment
      @comment ||= current_post.comments.build
    end

    def comment_params
      params.require(:comment).permit(:author, :content)
    end
end