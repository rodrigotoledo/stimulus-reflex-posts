module PostsConcern
  extend ActiveSupport::Concern

  protected

    def set_posts
      @posts ||= Post.with_cache
    end

    def post_params
      params.require(:post).permit(:title, :content)
    end
end