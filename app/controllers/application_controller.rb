class ApplicationController < ActionController::Base
  before_action :set_count_posts
  
  protected

    def set_count_posts
      @count_posts = Post.count
    end
end
