class Api::V1::CommentsController < ActionController::API
  include CommentsConcern

  helper_method :current_post
  helper_method :current_comment
  
  def index
    render json: current_post.comments.with_cache
  end

  def create
    current_comment.attributes = comment_params
    current_comment.save
    render json: { id: current_comment.id, author: current_comment.author, comment: current_comment.content, post_id: current_comment.post_id }
  end
end
