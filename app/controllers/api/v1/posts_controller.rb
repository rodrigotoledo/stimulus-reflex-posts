class Api::V1::PostsController < ActionController::API
  include PostsConcern

  before_action :set_posts, only: :index
  
  def index
    render json: @posts
  end

  def create
    post = Post.create(post_params)
    render json: { id: post.id, title: post.title, content: post.content }
  end
end
