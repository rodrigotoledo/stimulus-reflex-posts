class CommentsController < ApplicationController
  include CommentsConcern
  helper_method :current_post
  helper_method :current_comment
  
  def index
  end
end
