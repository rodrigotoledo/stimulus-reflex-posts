Rails.application.routes.draw do
  root "posts#index"

  namespace :api do
    namespace :v1 do
      resources :posts, only: [:index, :create] do
        resources :comments, only: [:index, :create]
      end
    end
  end

  resources :posts, only: [:index, :new] do
    resources :comments, only: [:index]
  end
end
